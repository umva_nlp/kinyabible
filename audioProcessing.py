#!/usr/bin/env python
# coding=utf-8

from aeneas.executetask import ExecuteTask
from aeneas.task import Task
from pydub import AudioSegment
import csv
import json
import pandas as pd
import os, shutil

class SyncMapper(object):

    @classmethod
    def process(cls,configFile, chaptersDir):
       # create a task object
        with open(configFile) as f:
            inputData = json.load(f)
            bibleVersion = inputData["bibleVersion"]
            bibleBooks = bibleVersion["books"]
            aeneasConfig = inputData["aeneas"]
            # task config
            config_string = u"task_language="+aeneasConfig["task_language"]+"|is_text_type=plain|os_task_file_format=json" 
            for book in bibleBooks.keys():
                numberOfChapters = bibleBooks[book]
                for chap in range(1,numberOfChapters + 1):
                    currentChapterDir = chaptersDir + book + "_" + str(chap) + "/"
                    filename = currentChapterDir  + book + "_" + str(chap)
                    task = Task(config_string=config_string)
                    # get data
                    task.audio_file_path_absolute = filename + ".mp3"
                    task.text_file_path_absolute = filename + ".txt"
                    task.sync_map_file_path_absolute = filename +"_" + aeneasConfig["task_language"] + ".json"
                    # process Task
                    ExecuteTask(task).execute()
                    # output sync map to file
                    task.output_sync_map_file()


class Segmenter(object):

    @classmethod
    def process(cls,configFile, chaptersDir, versesDir):
        if not os.path.exists(versesDir):
                os.makedirs(versesDir)
        df = pd.DataFrame(columns=['audio','text','duration'])

        with open(configFile) as f:
            inputData = json.load(f)
            bibleVersion = inputData["bibleVersion"]
            bibleBooks = bibleVersion["books"]
            aeneasConfig = inputData["aeneas"]

            count = 1
            for book in bibleBooks.keys():
                numberOfChapters = bibleBooks[book]
                for chap in range(1,numberOfChapters + 1):
                    currentChapterDir = chaptersDir + book + "_" + str(chap) + "/"
                    filename = currentChapterDir  + book + "_" + str(chap)
                    chapterAudio = AudioSegment.from_mp3(filename + ".mp3")

                    with open(filename + "_" + aeneasConfig["task_language"] + ".json") as f: 
                        syncmap = json.loads(f.read())
                        sentences = []
                        for fragment in syncmap['fragments']:
                            if ((float(fragment['end'])*1000) - float(fragment['begin'])*1000) > 400:
                                sentences.append({"audio":chapterAudio[float(fragment['begin'])*1000:float(fragment['end'])*1000], "text":fragment['lines'][0]})

                        for idx, sentence in enumerate(sentences):
                            if(idx>0):  # Escape the first fragment because it contains the introduction audio to the chapter without a transcript
                                text = sentence['text'].lower().encode("utf-8")
                                sentence['audio'].export(versesDir+"sample-"+str(count)+".mp3", format="mp3")
                                duration = len(AudioSegment.from_file(versesDir+"sample-"+str(count)+".mp3"))/1000 
                                temp_df = pd.DataFrame([{'audio':"sample-"+str(count)+".mp3",'text':text,'duration':duration}], columns=['audio','text','duration'])
                                df = df.append(temp_df)
                                df.to_csv(versesDir + "result.csv", index=False)
                                count += 1
                                print("sample " + str(count))
                                    

            