from bs4 import BeautifulSoup
import requests
import csv
import json
import os


class Parser(object):

    @classmethod
    def process(cls,configFile, outputDir):
        # safely create outputDir if it doesn't exit
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
        
        # Parse input data for scrapping
        with open(configFile) as f:
            inputData = json.load(f)
            bibleVersion = inputData["bibleVersion"]
            bibleContentBaseUrl = inputData["bibleContentBaseUrl"]
            cls.__bibleParsing(bibleVersion, bibleContentBaseUrl, outputDir)

    @classmethod
    def __bibleParsing(cls, bibleVersion, bibleContentBaseUrl, outputDir):
        bibleId = str(bibleVersion["id"])
        bibleAbbrev = bibleVersion["abbrev"]
        bibleName = bibleVersion["name"]
        bibleBooks = bibleVersion["books"]

        # foreach book in the bible books
        for book in bibleBooks.keys():
            numberOfChapters = bibleBooks[book]
            cls.__chapterParsing(bibleId, book, numberOfChapters, bibleContentBaseUrl, bibleAbbrev, outputDir)

    @classmethod
    def __chapterParsing(cls,bibleId, book, numberOfChapters,bibleContentBaseUrl, bibleAbbrev , outputDir):
        for chap in range(1,numberOfChapters + 1):
            chapterContentUrl = bibleContentBaseUrl + bibleId + "/" + book + "." + str(chap) + "." + bibleAbbrev
            chapterPage = requests.get(chapterContentUrl, timeout=40,verify=False)
            raw_html = chapterPage.text
            soup = BeautifulSoup(raw_html, 'html.parser')
            chapterAudioUrl =  cls.__chapterAudioParsing(soup)
            
            currentChapterDir = outputDir + book + "_" + str(chap)
            if not os.path.exists(currentChapterDir):
                os.makedirs(currentChapterDir)

            filename =  currentChapterDir + "/" + book + "_" + str(chap)

            if(chapterAudioUrl.split()):            
                cls.__downloadAudio(chapterAudioUrl, filename + ".mp3")
                chapFile = open(filename + ".txt","w+")
                chapterText = soup.find("div", {"class": "chapter"})
                if chapterText is not None:
                    chapterVerses = chapterText.findAll("span", {"class": "verse"})
                    # foreach verse in current chapter
                    for verse in chapterVerses:
                        verseText = cls.__verseParsing(verse)
                        if verseText.strip():
                            chapFile.write(verseText.encode("utf-8")+'\n')

    @classmethod
    def __verseParsing(cls, verse):
        verseNumber = verse["data-usfm"]
        verseContent = verse.find("span", {"class": "content"}).get_text()
        return verseContent

    @classmethod
    def __chapterAudioParsing(cls, soup):
        if len(soup.find_all("source")) > 0:
            chapterAudioUrl = "http:" + soup.find_all("source")[1]["src"]
        else:
            chapterAudioUrl = ""
        return chapterAudioUrl

    @classmethod
    def __downloadAudio(cls, url, filepath):
        doc = requests.get(url)
        with open(filepath, 'wb') as f:
            f.write(doc.content)

                            