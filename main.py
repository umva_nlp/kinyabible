from bibleWebContentProcessor import Parser
from audioProcessing import SyncMapper, Segmenter

bibleConfigFile = 'config.json'
outputDir = './dataset/'
chaptersDir = outputDir + "chapters/";
versesDir = outputDir + "verses/"

parseWebBible = True  # Set it to false if you have already parsed the web Bible content

# STEP 1: Parsing the web Bible to get the dataset of chapters (an audio for each bible chapter with its transcript)
if(parseWebBible):
    print("Parse and download text and audio of bible chapters")
    Parser.process(bibleConfigFile, chaptersDir)

# STEP 2: Force alignement of verse transcripts to their respective audio from audio chapters
print("Create sync maps to localize verse audios inside chapter audios")
SyncMapper.process(bibleConfigFile, chaptersDir)

# STEP 3: Generate verse datasets (audio for each verse and its transcript)
print('Create audio chunks of verses')
Segmenter.process(bibleConfigFile, chaptersDir, versesDir)