# Bible Dataset Preprocessing process

This process is carried out in 3 steps:
* STEP 1: Parsing and Download audio and text for bible chapters at "bible.com"
* STEP 2: Create a synchronisation map indicating the start and end time of verse audio inside chapter audio
* STEP 3: Generate audio chunks for verses

Before you start, make sure you have the appropriate execution environment

## Preparing the execution environment

You can install the required librairies directly on your OS or work in a docker environment (best option).

### Execution on your OS

You need to have python 2.7 and then install the following python libraries: 
"numpy", "pydub", "pandas", "bs4", "requests" and finally "aeneas".

### Execution in a docker environment

A ready to use docker image is available on Umva's docker hub. 
Run "docker pull umva/bible_preprocess_env". 
This image has been produced using the dockerfile located at "docker/Dockerfile".
Note that the docker image doesn't contain the processing scripts. 
They have to be inserted in a docker container via a monuted volume.

## Configuration

The configuration is defined in the config.json. It contains the following parameters of the process: 
* The base URL of the bible chapters on the website bible.com
* The bible version to be processed
* The aeneas configuration for audio segmentation

## Launch the process

The main script to be executed is "main.py". 

### Execution on your OS

Simply run "python main.py" ....

### Execution in a docker environment

* Create a container by running "docker run -it --rm --name bible_preprocess -v $(PWD):/app umva/bible_preprocess_env /bin/bash"

Then, once in the container:
* execute "python main.py"


## Generated datasets

At the end of the process, one gets a aataset organized in 2 main folders: 

* chapters/: contains different chapter folders. In each folder, there is the audio for the chapter, the corresponding transcript, the sync map per task language

* verses/: a metadata csv file that map each verse audio file to the corresponding verse text 
